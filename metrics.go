package main

import (
	"os"
	"strings"

	"github.com/prometheus/client_golang/prometheus"
)

var (
	requestCounter        *prometheus.CounterVec
	bytesSentCounter      *prometheus.CounterVec
	responseTimeHistogram *prometheus.HistogramVec
	hostname              string

	metricLabels = []string{"server", "method", "code", "target", "hostname"}
)

func init() {
	hostname = determineHostname()
}

func init() {
	requestCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "",
		Subsystem: "nginx",
		Name:      "requests_total",
		Help:      "total number of requests",
	}, metricLabels)

	bytesSentCounter = prometheus.NewCounterVec(prometheus.CounterOpts{
		Namespace: "",
		Subsystem: "nginx",
		Name:      "body_bytes_total",
		Help:      "total number of sent bytes",
	}, metricLabels)

	responseTimeHistogram = prometheus.NewHistogramVec(prometheus.HistogramOpts{
		Namespace: "",
		Subsystem: "nginx",
		Name:      "request_duration_seconds",
		Help:      "seconds for request to be processed",
	}, metricLabels)
}

// NewRegistry returns a new prometheus registry
func NewRegistry() *prometheus.Registry {
	r := prometheus.NewRegistry()
	r.MustRegister(requestCounter, bytesSentCounter, responseTimeHistogram)
	return r
}

func determineHostname() string {
	c, err := os.ReadFile("hostname")
	if err == nil {
		return strings.TrimSpace(string(c))
	}

	h := os.Getenv("HOSTNAME")
	if h != "" {
		return h
	}

	if h == "" {
		h, _ = os.Hostname()
	}

	return h
}
