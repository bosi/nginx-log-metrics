package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/prometheus/client_golang/prometheus/promhttp"
)

func main() {
	go func() {
		log.Fatal(watchLog())
	}()

	http.Handle("/metrics", promhttp.HandlerFor(NewRegistry(), promhttp.HandlerOpts{}))
	http.HandleFunc("/status", handleStatus)

	log.Println("start http server at port 9911")
	log.Fatal(http.ListenAndServe(":9911", nil))
}

func handleStatus(w http.ResponseWriter, _ *http.Request) {
	fmt.Fprintf(w, "ok")
}
