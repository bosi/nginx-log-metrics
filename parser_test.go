package main

import (
	"testing"
	"time"

	"github.com/stretchr/testify/assert"
)

func TestParse(t *testing.T) {
	t.Parallel()

	vt, _ := time.Parse("2006-01-02T15:04:05-07:00", "2020-02-15T08:23:41+00:00")
	vd, _ := time.ParseDuration("0.024s")

	scenarios := []struct {
		name     string
		logLine  string
		logEntry logEntry
		err      bool
	}{
		{
			name:     "bad structure",
			logLine:  "sovn",
			logEntry: logEntry{},
			err:      true,
		},
		{
			name:     "invalid date",
			logLine:  "951|myserver|GET|200|547|10.1.2.3:8080",
			logEntry: logEntry{},
			err:      true,
		},
		{
			name:     "invalid status code",
			logLine:  "2020-02-15T08:23:41+00:00|myserver|GET|200abc|547|10.1.2.3:8080",
			logEntry: logEntry{},
			err:      true,
		},
		{
			name:     "invalid status code",
			logLine:  "2020-02-15T08:23:41+00:00|myserver|GET|200|547abc|10.1.2.3:8080",
			logEntry: logEntry{},
			err:      true,
		},
		{
			name:     "valid log line without response time",
			logLine:  "2020-02-15T08:23:41+00:00|myserver|GET|200|547|10.1.2.3:8080",
			logEntry: logEntry{DateTime: vt, Server: "myserver", Method: "GET", StatusCode: 200, BytesSent: 547, ProxyTarget: "10.1.2.3:8080"},
			err:      false,
		},
		{
			name:     "invalid response time",
			logLine:  "2020-02-15T08:23:41+00:00|myserver|GET|200|547|10.1.2.3:8080|24abc",
			logEntry: logEntry{},
			err:      true,
		},
		{
			name:     "valid log",
			logLine:  "2020-02-15T08:23:41+00:00|myserver|GET|200|547|10.1.2.3:8080|0.024",
			logEntry: logEntry{DateTime: vt, Server: "myserver", Method: "GET", StatusCode: 200, BytesSent: 547, ProxyTarget: "10.1.2.3:8080", ResponseTime: vd},
			err:      false,
		},
	}

	for _, scenario := range scenarios {
		scenario := scenario
		t.Run(scenario.name, func(t *testing.T) {
			t.Parallel()

			le, err := parse(scenario.logLine)
			assert.Equal(t, scenario.logEntry, le)

			if scenario.err {
				assert.NotNil(t, err)
			} else {
				assert.Nil(t, err)
			}
		})
	}
}
