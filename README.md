<img src="https://gitlab.com/bosi/nginx-log-metrics/raw/master/logo.png" width="400" alt="Logo">

# Nginx Log Metrics

[![pipeline status](https://gitlab.com/bosi/nginx-log-metrics/badges/master/pipeline.svg)](https://gitlab.com/bosi/nginx-log-metrics/-/commits/master)
[![coverage report](https://gitlab.com/bosi/nginx-log-metrics/badges/master/coverage.svg)](https://gitlab.com/bosi/nginx-log-metrics/-/commits/master)

This project provides a simple go application which uses nginx access log to provide metrics for prometheus.

## General facts

### Why I start this project?

I am a big fan of reverse proxies and monitoring them. Nginx is a great tool for setting up a reverse proxy and already
has a build-in module for simple statistics about the service. My problem was that, if you don't use nginx plus, it's
really hard to get information about request-count and bytes sent divided by method, status-code or proxy-target. I just
want to know which backend service causes how much traffic and apply some basic filter to it.

### What was the goal of the project?

Since nginx already has a lot of information my goal was to fetch them and bring them in a useful format. I use
prometheus and grafana to monitor my services so a "useful" format in the first place are prometheus metrics.

### What exactly does the tool?

Basically it uses a defined log format for nginx to collect all necessary information from the access log and create an
http endpoint that delivers Prometheus metrics.

## Features

* provide metrics about request-count and bytes sent to clients
* metrics are labeled with http-method, status-code, proxy-target and the server which answered the request
* auto-cleanup of the used access-log
* no direct access to nginx necessary
* can use custom hostname as metric label

## Requirements

* a running nginx
* docker (only when using container)
* golang 1.18 (when compiled from source)

Note: I'll use docker-compose in my examples. If you want to copy-n-paste them you need docker-compose as well.

## Usage

### Quick start

* start example: `cd example && make start`
* generate some requests: `curl http://localhost`
* watch metrics: `curl http://localhost:9911/metrics`
* you may also watch metrics using prometheus via browser: `http://localhost:9090`

### Implement in your application

The following steps explain the very basic usage of this tool. Please also have a look at the example-dir and the
config-files inside.

1. add new logging-format to nginx `http`-section
2. apply additional logging with new logging-format to `server`-sections or globally to `http`-section
3. create logging file (important because otherwise docker will create a directory) or a logging directory
4. start your nginx and the nginx-log-metrics
5. get metrics for e.g. prometheus and grafana via `/metrics` endpoint of nginx-log-metrics

## Example PromQL-Queries

| description                                   | query                                                                                        |
|-----------------------------------------------|----------------------------------------------------------------------------------------------|
| requests (rate per minute) by server          | `sum(rate(nginx_requests_total[5m])*60) by (server)`                                         |
| transferred bytes (rate per minute) by server | `sum(rate(nginx_body_bytes_total[5m])*60) by (server)`                                       |
| avg response time                             | `sum(nginx_request_duration_seconds_sum / nginx_request_duration_seconds_count) by (server)` |