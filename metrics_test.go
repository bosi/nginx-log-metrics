package main

import (
	"fmt"
	"os"
	"testing"

	"github.com/stretchr/testify/require"

	"syreclabs.com/go/faker"
)

//nolint:paralleltest
func TestHostname(t *testing.T) {
	os.Remove("hostname")

	t.Run("check hostname from system", func(t *testing.T) {
		t.Setenv("HOSTNAME", "")
		hn, err := os.Hostname()
		require.Nil(t, err)
		require.Equal(t, determineHostname(), hn)
	})

	t.Run("check hostname from env", func(t *testing.T) {
		newHn := faker.Lorem().Word()
		t.Setenv("HOSTNAME", newHn)
		require.Equal(t, determineHostname(), newHn)
	})

	t.Run("check hostname from file", func(t *testing.T) {
		var err error
		f, err := os.Create("hostname")
		require.Nil(t, err)
		newHn := faker.Lorem().Word()
		_, err = fmt.Fprint(f, newHn)
		require.Nil(t, err)
		require.Equal(t, determineHostname(), newHn)
	})
}
