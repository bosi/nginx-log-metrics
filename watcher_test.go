package main

import (
	"testing"

	io_prometheus_client "github.com/prometheus/client_model/go"
	"github.com/stretchr/testify/assert"
)

func TestProcessLogLine(t *testing.T) {
	t.Parallel()

	ll1 := "2020-02-15T08:23:41+00:00|myserver|GET|200|547|10.1.2.3:8080|0.024"

	assert.Nil(t, processLogLine(ll1))
	m := &io_prometheus_client.Metric{}

	assert.Nil(t, requestCounter.WithLabelValues("myserver", "GET", "200", "10.1.2.3:8080", hostname).Write(m))
	assert.Equal(t, float64(1), m.GetCounter().GetValue())

	assert.Nil(t, bytesSentCounter.WithLabelValues("myserver", "GET", "200", "10.1.2.3:8080", hostname).Write(m))
	assert.Equal(t, float64(547), m.GetCounter().GetValue())
}
