package main

import (
	"io"
	"log"
	"os"
	"strings"
	"time"

	"github.com/papertrail/go-tail/follower"
)

const (
	maxPathDeterminationRetries = 5
	pathDeterminationRetryDelay = 2 * time.Second
)

var (
	logPath          string
	nextFileTruncate time.Time

	logfilePaths = []string{
		"/app/stats.log",
		"/app/logs/stats.log",
	}
)

func init() {
	nextFileTruncate = time.Now().Add(1 * time.Minute)
}

// watchLog starts a watcher on a log-file.
// It will also trigger processing new lines
func watchLog() error {
	log.Println("try to find stats logfile")
	for i := 0; i < maxPathDeterminationRetries; i++ {
		logPath = determineLofFilePath()
		if logPath != "" {
			break
		}

		time.Sleep(pathDeterminationRetryDelay)
	}

	if logPath == "" {
		log.Fatalf("could not find stats logfile in one of the following locations: %s\n", strings.Join(logfilePaths, ", "))
	}

	log.Println("start file watcher")
	t, err := follower.New(logPath, follower.Config{
		Whence: io.SeekEnd,
		Offset: 0,
		Reopen: true,
	})
	if err != nil {
		return err
	}

	for line := range t.Lines() {
		line := line
		go func() {
			if err := processLogLine(line.String()); err != nil {
				log.Printf("ERROR | %s", err.Error())
			}
		}()
	}

	panic("error while running file-watcher: watcher should not end")
}

func processLogLine(logLine string) error {
	le, err := parse(logLine)
	if err != nil {
		return err
	}

	updateMetrics(le)

	if time.Now().After(nextFileTruncate) {
		log.Println("truncate stat file")
		if err := os.Truncate(logPath, 0); err != nil {
			log.Printf("ERROR | %s", err.Error())
		}
		nextFileTruncate = time.Now().Add(1 * time.Minute)
	}

	return nil
}

func updateMetrics(le logEntry) {
	requestCounter.WithLabelValues(le.labels()...).Inc()
	bytesSentCounter.WithLabelValues(le.labels()...).Add(float64(le.BytesSent))

	if le.ResponseTime != 0 {
		responseTimeHistogram.WithLabelValues(le.labels()...).Observe(le.ResponseTime.Seconds())
	}
}

func determineLofFilePath() string {
	for _, path := range logfilePaths {
		if _, err := os.Stat(path); err == nil {
			return path
		}
	}

	return ""
}
