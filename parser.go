package main

import (
	"errors"
	"fmt"
	"strconv"
	"strings"
	"time"
)

type logEntry struct {
	DateTime     time.Time
	Server       string
	Method       string
	StatusCode   int
	BytesSent    int
	ProxyTarget  string
	ResponseTime time.Duration
}

const logParts = 6

func parse(logLine string) (logEntry, error) {
	parts := strings.Split(logLine, "|")
	if len(parts) < logParts {
		return logEntry{}, errors.New("log line does not have at lease 6 elements")
	}
	var err error

	le := logEntry{
		Server:      parts[1],
		Method:      parts[2],
		ProxyTarget: parts[5],
	}

	le.DateTime, err = time.Parse("2006-01-02T15:04:05-07:00", parts[0])
	if err != nil {
		return logEntry{}, errors.New("datetime cannot be parsed from iso8601: " + err.Error())
	}

	le.StatusCode, err = strconv.Atoi(parts[3])
	if err != nil {
		return logEntry{}, fmt.Errorf("\"%s\" could not be parsed as integer", parts[3])
	}

	le.BytesSent, err = strconv.Atoi(parts[4])
	if err != nil {
		return logEntry{}, fmt.Errorf("\"%s\" could not be parsed as integer", parts[4])
	}

	if len(parts) > logParts {
		le.ResponseTime, err = time.ParseDuration(parts[6] + "s")
		if err != nil {
			return logEntry{}, fmt.Errorf("\"%s\" could not be parsed as time duration", parts[6])
		}
	}

	return le, nil
}

func (le logEntry) labels() []string {
	return []string{
		le.Server,
		le.Method,
		strconv.Itoa(le.StatusCode),
		le.ProxyTarget,
		hostname,
	}
}
